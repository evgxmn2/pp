# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

User.create(last_name: 'Иванов', first_name: 'Иван', patronymic: 'Иванович', job_title: 'директор', email: 'ivanov@gmail.com')
User.create(last_name: 'Петров', first_name: 'Алексей', patronymic: 'Григорьевич', job_title: 'менеджер', email: 'petrov@gmail.com')
User.create(last_name: 'Сидоров', first_name: 'Александр', patronymic: 'Петрович', job_title: 'менеджер', email: 'sidorov@gmail.com')
User.create(last_name: 'Ершов', first_name: 'Григорий', patronymic: 'Евгеньевич', job_title: 'бухгалтер', email: 'ershov@gmail.com')
User.create(last_name: 'Пальцев', first_name: 'Артём', patronymic: 'Александрович', job_title: 'начальник литейки', email: 'palcev@gmail.com')
User.create(last_name: 'Тарасов', first_name: 'Вадим', patronymic: 'Алексеевич', job_title: 'литейщик', email: 'igor@gmail.com')
User.create(last_name: 'Кондратьева', first_name: 'Екатерина', patronymic: 'Владимировна', job_title: 'менеджер', email: 'dfg89@gmail.com')
User.create(last_name: 'Лысенко', first_name: 'Ольга', patronymic: 'Николаевна', job_title: 'HR', email: 'list77@gmail.com')
User.create(last_name: 'Леонтьева', first_name: 'Лидия', patronymic: 'Павловна', job_title: 'литейщик', email: 'kdr12@gmail.com')
