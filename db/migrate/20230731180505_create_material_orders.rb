class CreateMaterialOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :material_orders do |t|
      t.references :supplier, null: false, foreign_key: true

      t.string :name, null: false
      t.numeric :total_price

      t.timestamps
    end
  end
end
