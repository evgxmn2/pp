class CreateItems < ActiveRecord::Migration[7.0]
  def change
    create_table :items do |t|
      t.string :name, null: false
      t.numeric :average_price
      t.string :description
      t.decimal :weight, precision: 20, scale: 5
      t.decimal :length, precision: 20, scale: 5
      t.decimal :width, precision: 20, scale: 5
      t.decimal :height, precision: 20, scale: 5

      t.timestamps
    end
  end
end
