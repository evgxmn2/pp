class CreateMaterialOrderItems < ActiveRecord::Migration[7.0]
  def change
    create_table :material_order_items do |t|
      t.references :material, null: false, foreign_key: true
      t.references :material_order, null: false, foreign_key: true
      t.integer :quantity
      t.decimal :price, precision: 10, scale: 2
      t.integer :delivery_period

      t.timestamps
    end
  end
end
