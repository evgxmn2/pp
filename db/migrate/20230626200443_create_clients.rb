class CreateClients < ActiveRecord::Migration[7.0]
  def change
    create_table :clients do |t|
      t.string :name, null: false
      t.string :inn, unique: true, null: false
      t.string :address
      t.string :site
      t.string :email

      t.timestamps
    end

    add_index :clients, :inn
  end
end
