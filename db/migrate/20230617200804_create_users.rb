class CreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users do |t|
      t.string :first_name, null: false
      t.string :last_name
      t.string :patronymic
      t.string :job_title
      t.string :email, null: false
      t.string :role

      t.timestamps
    end

    add_index :users, :email, unique: true
  end
end
 