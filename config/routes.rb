# frozen_string_literal: true

Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ('/')
  # root 'articles#index'
  root to: 'react#home'

  get '/up', to: 'health_check#up'

  namespace :api do
    namespace :v1 do
      resources :users
    end
  end

  get 'api/v1/user/find_for_create', to: 'api/v1/users#find_for_create'
  get 'api/v1/user/find_for_edit', to: 'api/v1/users#find_for_edit'

  namespace :api do
    namespace :v1 do
      resources :clients do
        member do
          get :get_files
        end
      end
    end
  end

  get 'api/v1/client/find_for_create', to: 'api/v1/clients#find_for_create'
  get 'api/v1/client/find_for_edit', to: 'api/v1/clients#find_for_edit'

  get 'api/v1/file/destroy_by_url', to: 'api/v1/files#destroy_by_url'

  namespace :api do
    namespace :v1 do
      resources :suppliers do
        member do
          get :get_files
        end
      end
    end
  end

  get 'api/v1/supplier/find_for_create', to: 'api/v1/suppliers#find_for_create'
  get 'api/v1/supplier/find_for_edit', to: 'api/v1/suppliers#find_for_edit'

  namespace :api do
    namespace :v1 do
      resources :materials do
        member do
          get :get_files
        end
      end
    end
  end

  get 'api/v1/material/find_for_create', to: 'api/v1/materials#find_for_create'
  get 'api/v1/material/find_for_edit', to: 'api/v1/materials#find_for_edit'
  get 'api/v1/material/check_for_create_order', to: 'api/v1/materials#check_for_create_order'

  namespace :api do
    namespace :v1 do
      resources :material_orders do
        member do
          get :get_files
        end
      end
    end
  end

  get 'api/v1/material_order/find_for_create', to: 'api/v1/material_orders#find_for_create'
  get 'api/v1/material_order/find_for_edit', to: 'api/v1/material_orders#find_for_edit'

  namespace :api do
    namespace :v1 do
      resources :items do
        member do
          get :get_files
        end
      end
    end
  end

  get 'api/v1/item/find_for_create', to: 'api/v1/items#find_for_create'
  get 'api/v1/item/find_for_edit', to: 'api/v1/items#find_for_edit'

  namespace :api do
    namespace :v1 do
      resources :material_order_items
    end
  end

  # Route for Active Storage files
  # get "/rails/active_storage/*path", to: "active_storage/blobs#show"

  # Route for downloading attachments
  # get "/download_attachment/:id", to: "active_storage#download_attachment"

  get 'users/:id', to: 'react#home'
  get 'users/new', to: 'react#home'
  get 'users', to: 'react#home'
  get 'users/:id/edit', to: 'react#home'
  get 'clients/:id', to: 'react#home'
  get 'clients/new', to: 'react#home'
  get 'clients', to: 'react#home'
  get 'clients/:id/edit', to: 'react#home'
  get 'suppliers/:id', to: 'react#home'
  get 'suppliers/new', to: 'react#home'
  get 'suppliers', to: 'react#home'
  get 'suppliers/:id/edit', to: 'react#home'
  get 'materials/:id', to: 'react#home'
  get 'materials/new', to: 'react#home'
  get 'materials', to: 'react#home'
  get 'materials/:id/edit', to: 'react#home'
  get 'material_orders/:id', to: 'react#home'
  get 'material_orders/new', to: 'react#home'
  get 'material_orders', to: 'react#home'
  get 'material_orders/:id/edit', to: 'react#home'
  get 'items/new', to: 'react#home'
  get 'items', to: 'react#home'
  get 'items/:id/edit', to: 'react#home'
  get 'items/:id', to: 'react#home'

  # Route for React components
  # get "*path", to: "react#home"
end
