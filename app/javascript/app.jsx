import React from "react";
import ReactDOM from "react-dom/client";
import './components/i18n';
import {
    BrowserRouter,
    Routes,
    Route
} from "react-router-dom";

import MainPage from "./components/main_page"
import Users from "./components/users/users"
import UserNew from "./components/users/user_new"
import UserShow from "./components/users/user_show"
import UserEdit from "./components/users/user_edit"
import ClientNew from "./components/clients/client_new"
import ClientShow from "./components/clients/client_show"
import ClientEdit from "./components/clients/client_edit"
import Clients from "./components/clients/clients_index"
import SupplierNew from "./components/suppliers/supplier_new"
import SupplierShow from "./components/suppliers/supplier_show"
import SupplierEdit from "./components/suppliers/supplier_edit"
import Suppliers from "./components/suppliers/suppliers_index"
import MaterialNew from "./components/materials/material_new"
import MaterialShow from "./components/materials/material_show"
import MaterialEdit from "./components/materials/material_edit"
import Materials from "./components/materials/materials_index"
import MaterialOrderNew from "./components/material_orders/new"
import MaterialOrderShow from "./components/material_orders/show"
import MaterialOrdersIndex from "./components/material_orders/index"
import MaterialOrderEdit from "./components/material_orders/edit"
import ItemNew from "./components/items/new"
import ItemsIndex from "./components/items/index"

function App() {
    return (
        <div>
            <BrowserRouter>
            <Routes>            
                <Route path="*" element={<MainPage />} />
                <Route path="users/" element={<Users />} />
                <Route path="users/new" element={<UserNew />} />
                <Route path="users/:id/edit" element={<UserEdit />} />
                <Route path="users/:id" element={<UserShow />} />
                <Route path="clients/new" element={<ClientNew />} />
                <Route path="clients/:id" element={<ClientShow />} />
                <Route path="clients/:id/edit" element={<ClientEdit />} />
                <Route path="clients/" element={<Clients />} />
                <Route path="suppliers/new" element={<SupplierNew />} />
                <Route path="suppliers/:id" element={<SupplierShow />} />
                <Route path="suppliers/:id/edit" element={<SupplierEdit />} />
                <Route path="suppliers/" element={<Suppliers />} />
                <Route path="materials/new" element={<MaterialNew />} />
                <Route path="materials/" element={<Materials />} />
                <Route path="materials/:id" element={<MaterialShow />} />
                <Route path="materials/:id/edit" element={<MaterialEdit />} />
                <Route path="material_orders/new" element={<MaterialOrderNew />} />
                <Route path="material_orders/:id" element={<MaterialOrderShow />} />
                <Route path="material_orders/" element={<MaterialOrdersIndex />} />
                <Route path="material_orders/:id/edit" element={<MaterialOrderEdit />} />
                <Route path="items/new" element={<ItemNew />} />
                <Route path="items/" element={<ItemsIndex />} />
            </Routes>
            </BrowserRouter>
        </div>
    )
}

// Add some javascript to replace the div where = 'places-list-container'
// with com=ntent render above

const app = ReactDOM.createRoot(document.getElementById("App"));
app.render(<App />);