# frozen_string_literal: true

# Model for users
class User < ApplicationRecord
  validates :first_name, presence: true
  validates :email, presence: true, uniqueness: true
end
