# frozen_string_literal: true

# Model for clients
class Client < ApplicationRecord
  validates :name, presence: true
  validates :inn, presence: true, uniqueness: true

  has_many_attached :files
end
