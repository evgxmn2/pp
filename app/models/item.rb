# frozen_string_literal: true

# Model for items
class Item < ApplicationRecord
  validates :name, presence: true
end
