class MaterialOrderItem < ApplicationRecord
  belongs_to :material
  belongs_to :material_order
end
