class Supplier < ApplicationRecord
  validates :name, presence: true
  validates :inn, presence: true, uniqueness: true

  has_many_attached :files
  has_many :material_orders, dependent: :destroy # выберите один из вариантов dependent
  
end
