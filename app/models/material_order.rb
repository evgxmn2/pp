# frozen_string_literal: true

# Model for MaterialOrder
class MaterialOrder < ApplicationRecord
  has_many :material_order_items, dependent: :destroy
  has_many :materials, through: :material_order_items
  belongs_to :supplier
  has_many_attached :files
end
