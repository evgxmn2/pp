# frozen_string_literal: true
# Model for Material
class Material < ApplicationRecord
  validates :name, presence: true
  has_many_attached :files
  has_many :material_order_items
  has_many :material_orders, through: :material_order_items
end
