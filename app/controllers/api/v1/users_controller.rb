# frozen_string_literal: true

# Controller the API UserssController
module Api
  module V1
    # UsersController
    class UsersController < BaseController
      def user_destroy_session
        reset_session
        redirect_to root_path
      end

      def index
        users = get_users(params).order(last_name: :asc)

        render(json: { users: })
      end

      def show
        user = User.find(params[:id])

        render(json: { user: })
      end

      def create
        if User.create(user_params)
          render(json: {}, status: :created)
        else
          render json: { error: user.errors.messages }, status: 422
        end
      end

      def update
        user = User.find(params[:id])

        if user.update(user_params)
          render(json: {}, status: :created)
        else
          render json: { error: user.errors.messages }, status: 422
        end
      end

      def destroy
        user = User.find(params[:id])

        if user.destroy
          render(json: {}, status: ok)
        else
          render json: { error: user.errors.messages }, status: unprocessable_entity
        end
      end

      def find_for_create
        email_valid = User.where(email: params['input_email']).empty?

        render(json: { email_valid: })
      end

      def find_for_edit
        user_edit = User.find(params[:id])
        email_valid = User.where(email: params['input_email']).empty?
        email_valid = true if params[:count_edit_email] == '0' || user_edit.email == params['input_email']

        render(json: { email_valid:, user_edit: })
      end

      private

      def user_params
        params.require(:user).permit(:first_name, :last_name, :patronymic, :email, :job_title)
      end

      def get_users(params)
        query_params = {}

        query_params[:first_name] = "%#{params[:first_name]}%" if params[:first_name].present?
        query_params[:last_name] = "%#{params[:last_name]}%" if params[:last_name].present?
        query_params[:patronymic] = "%#{params[:patronymic]}%" if params[:patronymic].present?
        query_params[:job_title] = "%#{params[:job_title]}%" if query_params[:job_title].present?
        query_params[:email] = "%#{params[:email]}%" if query_params[:email].present?

        query_params = {} if query_params.blank?

        if query_params.blank?
          User.all
        else
          User.where("first_name ILIKE ? OR last_name ILIKE ? OR patronymic ILIKE ? OR job_title ILIKE ? OR email ILIKE ?", query_params[:first_name], query_params[:last_name], query_params[:patronymic], query_params[:job_title], query_params[:email])
        end
      end
    end
  end
end
