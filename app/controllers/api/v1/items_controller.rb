# frozen_string_literal: true

# Controller the API ItemsController
module Api
  module V1
    # ItemsController
    class ItemsController < BaseController
      def create
        item = item_new(params[:item])
        files = params[:item][:files]

        if item.save
          files&.each do |f|
            item.files.attach(io: f, filename: f.original_filename, content_type: f.content_type)
          end

          render(json: {}, status: :created)
        else
          render json: { error: item.errors.messages }, status: `422`
        end
      end

      def find_for_create
        name_valid = find_name_valid(params['count_name'], params['name'])

        render(json: { name_valid: })
      end

      private

      def item_new(i)
        Item.new(name: i[:name],
                 weight: i[:weight],
                 length: i[:length],
                 width: i[:width],
                 height: i[:height],
                 average_price: i[:average_price],
                 description: i[:description]                 
                )
      end

      def find_name_valid(count_name, name)
        if count_name == '0'
          true
        else
          Item.where(name:).empty?
        end
      end
    end
  end
end
